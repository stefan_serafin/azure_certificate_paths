```
******************************************************************************** 

Certification path for:
   Azure AI Engineer Associate
   Designing and Implementing an Azure AI Solution (AI-100)

( 1 )  Auswerten von Text mit dem Sprachdienst von Azure Cognitive Services
  ( 1 . 1 )  Klassifizieren und Moderieren von Text mit Azure Content Moderator
  ( 1 . 2 )  Hinzufügen von Unterhaltungsintelligenz zu Ihren Apps mithilfe des Language Understanding Intelligent Service (LUIS)
  ( 1 . 3 )  Ermitteln der Stimmung in Text mit der Textanalyse-API
( 2 )  Erstellen von intelligenten Bots mit Azure Bot Service
  ( 2 . 1 )  Erstellen eines Bots mit dem QnA Maker und dem Azure Bot Service
( 3 )  Verarbeiten und Klassifizieren von Bildern mit der Bildanalyse von Azure Cognitive Services
  ( 3 . 1 )  Identifizieren von Gesichtern und Gesichtsausdrücken mit der Maschinelles Sehen-API in Azure Cognitive Services
  ( 3 . 2 )  Verarbeiten von Bildern mit dem Maschinelles Sehen-Dienst
  ( 3 . 3 )  Klassifizieren von Bildern mit dem Microsoft Custom Vision Service
  ( 3 . 4 )  Auswerten der Anforderungen für die Implementierung der Custom Vision-APIs
******************************************************************************** 

Certification path for:
   Azure Solutions Architect Expert
   Microsoft Azure Architect Technologies (AZ-303)
   Microsoft Azure Architect Design (AZ-304)

( 1 )  Entwerfen einer Computeinfrastruktur in Azure
  ( 1 . 1 )  Verbinden Ihres lokalen Netzwerks mit Azure über VPN Gateway
  ( 1 . 2 )  Herstellen einer Verbindung zwischen dem lokalen Netzwerk und dem globalen Microsoft-Netzwerk mithilfe von ExpressRoute
  ( 1 . 3 )  Sichern und Isolieren des Zugriffs auf Azure-Ressourcen mithilfe von Netzwerksicherheitsgruppen und Dienstendpunkten
  ( 1 . 4 )  Verteilen von Diensten in virtuellen Azure-Netzwerken und Integrieren dieser Dienste über Peering virtueller Netzwerke
  ( 1 . 5 )  Verbessern der Dienstverfügbarkeit und Datenlokalität mithilfe von Azure Traffic Manager
  ( 1 . 6 )  Verbessern der Skalierbarkeit und Resilienz von Anwendungen mithilfe von Azure Load Balancer
  ( 1 . 7 )  Durchführen eines Lastenausgleichs für Ihren Webdienstdatenverkehr mit Application Gateway
  ( 1 . 8 )  Verwalten und Steuern des Datenverkehrsflusses mit Routen in Ihrer Azure-Bereitstellung
  ( 1 . 9 )  Entwerfen eines IP-Adressierungsschemas für Ihre Azure-Bereitstellung
  ( 1 . 10 )  Entwerfen einer hybriden Netzwerkarchitektur in Azure
  ( 1 . 11 )  Zentralisieren von Kerndiensten mithilfe der Hub-Spoke-Architektur virtueller Azure-Netzwerke
  ( 1 . 12 )  Verwenden von Netzwerküberwachungstools zum Überwachen der gesamten Azure-Netzwerkinfrastruktur und zur Problembehandlung
( 2 )  Entwerfen einer Speicherinfrastruktur in Azure
  ( 2 . 1 )  Auswählen einer geeigneten Strategie für die Speicherung von Daten in Azure
  ( 2 . 2 )  Erstellen eines Azure-Speicherkontos
  ( 2 . 3 )  Hochladen, Herunterladen und Verwalten von Daten mit dem Azure Storage-Explorer
  ( 2 . 4 )  Herstellen einer Verbindung zwischen einer App und Azure Storage
  ( 2 . 5 )  Hochverfügbarkeit für Ihren Anwendungsspeicher mithilfe des georedundanten Speichers mit Lesezugriff
  ( 2 . 6 )  Sichern Ihres Azure-Speicherkontos
  ( 2 . 7 )  Speichern und Freigeben von Dateien in Ihrer Anwendung mit Azure Files
  ( 2 . 8 )  Auswählen des richtigen Datenspeichers für die Arbeitsauslastung Ihres virtuellen Computers
  ( 2 . 9 )  Überwachung, Diagnose und Problembehandlung in Azure Storage
( 3 )  Entwerfen einer Computeinfrastruktur in Azure
  ( 3 . 1 )  Core Cloud Services – Computeoptionen in Azure
  ( 3 . 2 )  Verwalten von virtuellen Computern mit der Azure CLI
  ( 3 . 3 )  Auswählen einer Compute-Bereitstellungslösung für Ihre Anwendung
  ( 3 . 4 )  Erstellen von Azure Resource Manager-Vorlagen
  ( 3 . 5 )  Bereitstellen von virtuellen Azure-Computern über VHD-Vorlagen
  ( 3 . 6 )  Erstellen einer skalierbaren Anwendung mit VM-Skalierungsgruppen
  ( 3 . 7 )  Auswählen des besten Azure-Diensts für die Automatisierung von Geschäftsprozessen
  ( 3 . 8 )  Einführung in High Performance Computing (HPC) in Azure
  ( 3 . 9 )  Ausführen von parallelen Aufgaben in Azure Batch mit der Azure CLI
( 4 )  Entwerfen von Infrastrukturvorgängen in Azure
  ( 4 . 1 )  Steuern und Organisieren von Azure-Ressourcen mit Azure Resource Manager
  ( 4 . 2 )  Anwenden und Überwachen von Infrastrukturstandards mit Azure Policy
  ( 4 . 3 )  Entwerfen einer ganzheitlichen Überwachungsstrategie in Azure
  ( 4 . 4 )  Verbessern der Reaktion auf Incidents mithilfe von Warnungen in Azure
  ( 4 . 5 )  Analysieren Ihrer Azure-Infrastruktur mit Azure Monitor-Protokollen
( 5 )  Entwerfen einer Datenplattform im Azure
  ( 5 . 1 )  Core Cloud Services – Azure Datenspeicherungsoptionen
  ( 5 . 2 )  Bereitstellen einer Azure SQL-Datenbank zum Speichern von Anwendungsdaten
  ( 5 . 3 )  Skalieren mehrerer Azure SQL-Datenbanken mit Pools für elastische SQL-Datenbanken
  ( 5 . 4 )  Migrieren der in SQL Server gespeicherten relationalen Daten zu einer Azure SQL-Datenbank-Instanz
  ( 5 . 5 )  Entwickeln und Konfigurieren einer ASP.NET-Anwendung zum Abfragen einer Azure SQL-Datenbank
  ( 5 . 6 )  Erstellen einer skalierbaren Azure Cosmos DB-Datenbank
  ( 5 . 7 )  Globales Verteilen Ihrer Daten mit Azure Cosmos DB
  ( 5 . 8 )  Einfügen und Abfragen von Daten in Ihrer Azure Cosmos DB-Datenbank
  ( 5 . 9 )  Entwerfen eines Data Warehouse mit Azure Synapse Analytics
  ( 5 . 10 )  Importieren von Daten in Azure Synapse Analytics mithilfe von PolyBase
( 6 )  Entwerfen von Nachrichtenbrokern und serverlosen Anwendungen in Azure
  ( 6 . 1 )  Erstellen von serverloser Logik mit Azure Functions
  ( 6 . 2 )  Automatisches Weiterleiten und Verarbeiten von Daten mit Logic Apps
  ( 6 . 3 )  Auswahl eines Nachrichtenmodells in Azure zum Herstellen loser Verbindungen mit Ihren Diensten
  ( 6 . 4 )  Kommunikation zwischen Anwendungen mit Azure Queue Storage
  ( 6 . 5 )  Aktivieren von zuverlässigem Messaging für Big Data-Anwendungen mithilfe von Azure Event Hubs
  ( 6 . 6 )  Implementieren von nachrichtenbasierten Kommunikationsworkflows mit Azure Service Bus
  ( 6 . 7 )  Reagieren auf Zustandsänderungen in Ihren Azure-Diensten mithilfe von Event Grid
  ( 6 . 8 )  Sicheres Verfügbarmachen von Hybriddiensten mit Azure Relay
( 7 )  Entwerfen moderner Anwendungen in Azure
  ( 7 . 1 )  Erstellen einer containerisierten Webanwendung mit Docker
  ( 7 . 2 )  Ausführen von Docker-Containern mit Azure Container Instances
  ( 7 . 3 )  Erstellen und Speichern von Containerimages mit Azure Container Registry
  ( 7 . 4 )  Hosten einer Webanwendung mit Azure App Service
  ( 7 . 5 )  Ausführen eines Hintergrundtasks in einer App Service-Web-App mit WebJobs
  ( 7 . 6 )  Bereitstellen und Ausführen einer containerisierten Web-App mit Azure App Service
  ( 7 . 7 )  Einführung in Azure Kubernetes Service
  ( 7 . 8 )  Verwenden von veränderlichen und partiellen Daten in Azure Cache for Redis
( 8 )  Entwerfen einer API-Integration in Azure
  ( 8 . 1 )  Veröffentlichen und Verwalten von APIs mit Azure API Management
  ( 8 . 2 )  Verbessern der Leistung einer API durch Hinzufügen einer Cacherichtlinie in Azure API Management
  ( 8 . 3 )  Schützen von APIs in Azure API Management
  ( 8 . 4 )  Verwalten der Authentifizierung bei APIs mit Azure API Management
  ( 8 . 5 )  Verfügbarmachen mehrerer Azure-Funktions-Apps als einheitliche API mithilfe von Azure API Management
( 9 )  Entwerfen von Migration, Geschäftskontinuität und Notfallwiederherstellung in Azure
  ( 9 . 1 )  Entwerfen der Migration zu Azure
  ( 9 . 2 )  Schützen Ihrer lokalen Infrastruktur vor Notfällen mit Azure Site Recovery
  ( 9 . 3 )  Schützen Ihrer Azure-Infrastruktur mit Azure Site Recovery
  ( 9 . 4 )  Schützen Ihrer virtuellen Computer mithilfe von Azure Backup
  ( 9 . 5 )  Sichern und Wiederherstellen einer Azure SQL-Datenbank-Instanz
  ( 9 . 6 )  Erstellen einer skalierbaren Anwendung mit VM-Skalierungsgruppen
  ( 9 . 7 )  Erfüllen von Web-App-Leistungsanforderungen mit Regeln zur automatischen und dynamischen Skalierung
  ( 9 . 8 )  Skalieren einer App Service-Web-App zum effizienten Erfüllen der Anforderungen mit zentraler Hochskalierung und horizontaler Skalierung in App Service
  ( 9 . 9 )  Entwerfen einer geografisch verteilten Anwendung
******************************************************************************** 

Certification path for:
   Azure Data Scientist Associate
   Designing and Implementing a Data Science Solution on Azure (DP-100)

( 1 )  Erstellen von Machine Learning-Modellen
  ( 1 . 1 )  Explore and analyze data with Python
  ( 1 . 2 )  Train and evaluate regression models
  ( 1 . 3 )  Train and evaluate classification models
  ( 1 . 4 )  Train and evaluate clustering models
  ( 1 . 5 )  Train and evaluate deep learning models
( 2 )  Erstellen von Vorhersagemodellen ohne Code mit Azure Machine Learning
  ( 2 . 1 )  Verwenden des automatisierten maschinellen Lernens in Azure Machine Learning
  ( 2 . 2 )  Erstellen eines Regressionsmodells mithilfe des Azure Machine Learning-Designers
  ( 2 . 3 )  Erstellen eines Klassifizierungsmodells mit dem Azure Machine Learning-Designer
  ( 2 . 4 )  Erstellen eines Clustermodells mithilfe des Azure Machine Learning-Designers
( 3 )  Erstellen von KI-Lösungen mit Azure Machine Learning
  ( 3 . 1 )  Einführung in Azure Machine Learning
  ( 3 . 2 )  Trainieren eines Machine Learning-Modells mit Azure Machine Learning
  ( 3 . 3 )  Work with Data in Azure Machine Learning
  ( 3 . 4 )  Work with Compute in Azure Machine Learning
  ( 3 . 5 )  Orchestrate machine learning with pipelines
  ( 3 . 6 )  Bereitstellen von Machine Learning-Modellen mit Azure Machine Learning
  ( 3 . 7 )  Deploy batch inference pipelines with Azure Machine Learning
  ( 3 . 8 )  Tune hyperparameters with Azure Machine Learning
  ( 3 . 9 )  Automatisieren der ML-Modellauswahl mit Azure Machine Learning
  ( 3 . 10 )  Explain machine learning models with Azure Machine Learning
  ( 3 . 11 )  Detect and mitigate unfairness in models with Azure Machine Learning
  ( 3 . 12 )  Monitor models with Azure Machine Learning
  ( 3 . 13 )  Monitor data drift with Azure Machine Learning
```