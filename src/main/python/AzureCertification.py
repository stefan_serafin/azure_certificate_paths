from selenium import webdriver
from bs4 import BeautifulSoup
from Certification import Certification

certifications = [
    Certification([
        "Azure AI Engineer Associate",
        "Designing and Implementing an Azure AI Solution (AI-100)"
    ], [
        "https://docs.microsoft.com/de-de/learn/paths/evaluate-text-with-language-services/",
        "https://docs.microsoft.com/learn/paths/create-bots-with-the-azure-bot-service/",
        "https://docs.microsoft.com/learn/paths/classify-images-with-vision-services/"
    ]),
    Certification([
        "Azure Solutions Architect Expert",
        "Microsoft Azure Architect Technologies (AZ-303)",
        "Microsoft Azure Architect Design (AZ-304)"
    ], [
        "https://docs.microsoft.com/learn/paths/architect-network-infrastructure/",
        "https://docs.microsoft.com/learn/paths/architect-storage-infrastructure/",
        "https://docs.microsoft.com/learn/paths/architect-compute-infrastructure/",
        "https://docs.microsoft.com/learn/paths/architect-infrastructure-operations/",
        "https://docs.microsoft.com/learn/paths/architect-data-platform/",
        "https://docs.microsoft.com/learn/paths/architect-messaging-serverless/",
        "https://docs.microsoft.com/learn/paths/architect-modern-apps/",
        "https://docs.microsoft.com/learn/paths/architect-api-integration/",
        "https://docs.microsoft.com/learn/paths/architect-migration-bcdr/"
    ]),
    Certification([
        "Azure Data Scientist Associate",
        "Designing and Implementing a Data Science Solution on Azure (DP-100)"
    ], [
        "https://docs.microsoft.com/learn/paths/create-machine-learn-models/",
        "https://docs.microsoft.com/learn/paths/create-no-code-predictive-models-azure-machine-learning/",
        "https://docs.microsoft.com/learn/paths/build-ai-solutions-with-azure-ml-service/"
    ])
]

driver = webdriver.Chrome("D:/Projects/azure_certification_overview/resources/chromedriver.exe")


def print_certification_details(certification):
    print("*" * 80, "\n", file=output_file)
    print("Certification path for:", file=output_file)
    for certification_name in certification.name:
        print("  ", certification_name, file=output_file)

    print(file=output_file)

    level_1 = 1
    for url in certification.urls:
        driver.get(url)

        content = driver.page_source
        soup = BeautifulSoup(content, features="html.parser")
        headers = []

        x = soup.find("h1", class_="title is-2 has-margin-none has-margin-right-super-large-tablet")
        print("(", level_1, ") ", x.decode_contents(), file=output_file)

        level_2 = 1
        for a in soup.find_all("h3", class_="is-size-h6 has-margin-none has-content-margin-right-super-large-tablet"):
            print("  (", level_1, ".", level_2, ") ", a.decode_contents(), file=output_file)
            level_2 = level_2 + 1

        level_1 = level_1 + 1


output_file = open("../../../azure_certification_paths.txt", "w")
for certification in certifications:
    print_certification_details(certification)
output_file.close()

